<?php
/**
 * @version		$Id: login.php 4343 2016-06-01 10:18:23Z mic $
 * @package		Language Translation German Backend
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		GPL - www.gnu.org/copyleft/gpl.html
 */

// header
$_['heading_title']	= 'Verwaltung';

// Text
$_['text_heading']	= 'Verwaltung';
$_['text_login']	= 'Anmeldung Verwaltung';
$_['text_forgotten']= 'Passwort vergessen';

// Entry
$_['entry_username']= 'Benutzername';
$_['entry_password']= 'Passwort';

// Button
$_['button_login']	= 'Anmelden';

// Error
$_['error_login']	= 'Benutzername und/oder Passwort falsch';
$_['error_token']	= 'Sitzung ist abgelaufen - bitte nochmal anmelden';