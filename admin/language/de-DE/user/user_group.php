<?php
/**
 * @version		$Id: user_group.php 4343 2016-06-01 10:18:23Z mic $
 * @package		Language Language Translation German Backend
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		GPL - www.gnu.org/copyleft/gpl.html
 */

// Heading
$_['heading_title']		= 'Benutzergruppe';

// Text
$_['text_success']		= 'Einstellungen erfolgreich bearbeitet';
$_['text_list']			= 'Übersicht';
$_['text_add']			= 'Neu';
$_['text_edit']			= 'Bearbeiten';

// Column
$_['column_name']		= 'Name';
$_['column_action']		= 'Aktion';

// Entry
$_['entry_name']		= 'Name';
$_['entry_access']		= 'Zugriffsrechte<br /><b style="color:green;">Aufrufen</b>';
$_['entry_modify']		= 'Zugriffsrechte<br /><b style="color:red;">Ändern</b>';

// Error
$_['error_permission']	= 'Keine Rechte für diese Aktion';
$_['error_name']		= 'Benutzergruppename muss zwischen 3 und 64 Zeichen lang sein';
$_['error_user']		= 'Benutzergruppe beinhaltet aktuell %s Benutzer und kann daher nicht gelöscht werden';