<?php
// HTTP
define('HTTP_SERVER', 'http://newevo.loc/admin/');
define('HTTP_CATALOG', 'http://newevo.loc/');

// HTTPS
define('HTTPS_SERVER', 'http://newevo.loc/admin/');
define('HTTPS_CATALOG', 'http://newevo.loc/');

// DIR
define('DIR_APPLICATION', 'C:\OpenServer\domains\newevo2/admin/');
define('DIR_SYSTEM', 'C:\OpenServer\domains\newevo2/system/');
define('DIR_LANGUAGE', 'C:\OpenServer\domains\newevo2/admin/language/');
define('DIR_TEMPLATE', 'C:\OpenServer\domains\newevo2/admin/view/template/');
define('DIR_CONFIG', 'C:\OpenServer\domains\newevo2/system/config/');
define('DIR_IMAGE', 'C:\OpenServer\domains\newevo2/image/');
define('DIR_CACHE', 'C:\OpenServer\domains\newevo2/system/cache/');
define('DIR_DOWNLOAD', 'C:\OpenServer\domains\newevo2/system/download/');
define('DIR_UPLOAD', 'C:\OpenServer\domains\newevo2/system/upload/');
define('DIR_LOGS', 'C:\OpenServer\domains\newevo2/system/logs/');
define('DIR_MODIFICATION', 'C:\OpenServer\domains\newevo2/system/modification/');
define('DIR_CATALOG', 'C:\OpenServer\domains\newevo2/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'newevo');
define('DB_PREFIX', 'oc_');
