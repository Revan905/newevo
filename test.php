<?php

class ShopProduct implements Chargeable
{
    private $title = "Usual good";
    private $producerMainName = "Datsko";
    private $producerFirstName = "Taras";
    protected $price = 0;
    private $discount = 0;
    private $id = 0;
    const AVAILABLE = 0;
    const OUT_OF_STOCK = 1;

    function __construct($title, $firstName, $mainName, $price)
    {
        $this->title = $title;
        $this->producerFirstName = $firstName;
        $this->producerMainName = $mainName;
        $this->price = $price;
    }

    public function getProducerFirstName()
    {
        return $this->producerFirstName;
    }

    public function getProducerMainName()
    {
        return $this->producerMainName;
    }

    public function setDiscount($num)
    {
        return $this->discount = $num;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function getPrice()
    {
        return ($this->price - $this->discount);
    }

    function getProducer()
    {
        return "{$this->producerFirstName}"
        . "{$this->producerMainName }";
    }

    function GetSummaryLine()
    {
        $base = "{$this->title} ( {$this->producerMainName}";
        $base .= "{$this->producerFirstName} )";
        return $base;

    }

//$db = sqlite("mydb.db");
//$query_table = sqlite_query($db, "
//    CREATE TABLE products (
//    id INTEGER PRIMARY KEY AUTOINCREMENT,
//    type TEXT,
//    firstname TEXT,
//    mainname TEXT,
//    title TEXT,
//    price float,
//    numapges int,
//    playlength int,
//    discount int
//    )");
//    public function setID($id)
//    {
//        $this->id = $id;
//    }
//
//    public static function getInstance($id, PDO $pdo)
//    {
//        $stmt = $pdo->prepare("select * from product where id = ?");
//        $result = $stmt->execute(array($id));
//        $row = $stmt->fetch();
//        if (empty ($row)) {
//            return null;
//        }
//        if ($row['type'] === "book") {
//            $product = new BookProduct (
//                $row['title'],
//                $row['firstname'],
//                $row['mainname'],
//                $row['price'],
//                $row['numpages']
//            );
//        } else if ($row['type'] === "cd") {
//            $product = new CDProduct(
//                $row['title'],
//                $row['firstname'],
//                $row['mainname'],
//                $row['price'],
//                $row['playlength']
//            );
//
//        } else {
//            $product = new shopProduct(
//                $row['title'],
//                $row['firstname'],
//                $row['mainname'],
//                $row['price']
//            );
//        }
//    }
//
}


class StaticExample {
    static public $aNum = 0;
    static public function sayHello() {
        self::$aNum++;
        print "hello (".self::$aNum.")\n";
    }
}
abstract class ShopProductWriter
{
    protected $products = array();

    public function addProduct(shopProduct $shopProduct)
    {
        $this->products[] = $shopProduct;
    }
    abstract public function write();

}
class XmlProductWriter extends ShopProductWriter{
    public function write() {
        $writer =new XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0','UTF-8');
        $writer->startElement("products");
        foreach ( $this->products as $shopProduct ) {
            $writer->startElement("product");
            $writer->writeAttribute( "title", $shopProduct->getTitle() );
            $writer->startElement("summary");
            $writer->text( $shopProduct->getSummaryLine() );
            $writer->endElement(); // summary
            $writer->endElement(); // product
        }
        $writer->endElement(); // products
        $writer->endDocument();
        print $writer->flush();
    }
}
class TextProductWriter extends ShopProductWriter{
    public function write() {
        $str = "PRODUCTS:\n";
        foreach ( $this->products as $shopProduct ) {
            $str .= $shopProduct->getSummaryLine()."\n";
        }
        print $str;
    }
}

class CDProduct extends ShopProduct
{
    private $playLength = 0;

    function __construct($title, $firstName, $mainName, $price, $playLength)
    {
        parent::__construct($title, $firstName, $mainName, $price);
        $this->playLength = $playLength;
    }

    function GetPlayLength()
    {
        return $this->playLength;
    }

    function GetSummaryLine()
    {
        $base = parent::GetSummaryLine();
        $base .= ": play time - {$this->playLength} )";
        return $base;
    }
}

class BookProduct extends ShopProduct implements Chargeable
{
    private $numPages = 0;

    function __construct($title, $firstName, $mainName, $price, $numPages)
    {
        parent::__construct($title, $firstName, $mainName, $price);
        $this->numPages = $numPages;
    }

    function GetNumberOfPages()
    {
        return $this->numPages;
    }

    function GetSummaryLine()
    {
        $base = parent::GetSummaryLine();
        $base .= ": {$this->numPages} стр. )";
        return $base;

    }

    public function getPrice()
    {
        return $this->price;
    }

}
interface Chargeable{
    public function getPrice();
}


print StaticExample::$aNum;
StaticExample::sayHello();
$product1 = new ShopProduct("dsjkcdskjc", "t", "cd", 15);
//$product2 = new ShopProduct();
//var_dump($productl);
//var_dump($product2);
//print "Author: {$product1->getProducer()} \n";
//$writer->write(new Wrong());
$cd = new CDProduct("Porno", "Jenna", "Jameson", "500", "140");
print shopProduct::AVAILABLE;
$XML = new XMLProductWriter();
$XML->write();
?>