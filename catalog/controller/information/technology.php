<?php

class ControllerInformationTechnology extends Controller
{
    public function index() {
        $this->load->model('design/layout');
        $this->load->model('catalog/information');

        $this->language->load('information/technology'); //Optional. This calls for your language file

        $this->document->setTitle($this->language->get('heading_title')); //Optional. Set the title of your web page.



        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/static'),
            'separator' => $this->language->get('text_separator')
        );
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        // Text from language file
        $data['heading_title'] = $this->language->get('heading_title'); //Get "heading title"
        $data['text_content'] = $this->language->get('text_content');
        // set title of the page
        $this->document->setTitle("My Custom Page");

        // define children templates

        // set data to the variable
       $data['my_custom_text'] = "This is my custom page.";

        // call the "View" to render the output
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/technology.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/technology.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/information/technology.tpl', $data));
        }

    }
}
?>