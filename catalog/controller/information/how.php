<?php

class ControllerInformationHow extends Controller
{
    public function index() {
        $this->load->model('design/layout');
        $this->load->model('catalog/information');

        $this->language->load('information/how'); //Optional. This calls for your language file

        $this->document->setTitle($this->language->get('heading_title')); //Optional. Set the title of your web page.



        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home'),
            'separator' => false
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/static'),
            'separator' => $this->language->get('text_separator')
        );
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        // Text from language file
        $data['heading_title'] = $this->language->get('heading_title'); //Get "heading title"
        $data['text_content'] = $this->language->get('text_content');


        // call the "View" to render the output
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/how.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/how.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/information/how.tpl', $data));
        }

    }
}
?>