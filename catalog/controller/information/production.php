<?php
class ControllerInformationProduction extends Controller {
    public function index() {
        $this->document->setTitle($this->config->get('config_meta_title'));
        $this->document->setDescription($this->config->get('config_meta_description'));
        $this->document->setKeywords($this->config->get('config_meta_keyword'));
        $this->language->load('information/production');

        if (isset($this->request->get['route'])) {
            $this->document->addLink(HTTP_SERVER, 'canonical');
        }
        $data['heading_title'] = $this->language->get('heading_title'); //Get "heading title"
        $data['text_content'] = $this->language->get('text_content');

//		$data['column_left'] = $this->load->controller('common/column_left');
//		$data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['blog'] = $this->load->controller('blog/blog_block');


        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/production.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/production.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('EVO/template/information/production.tpl', $data));
        }
    }
}