<?php

class ControllerModuleCategoryformain extends Controller
{
    public function index($setting)
    {

        $this->load->language('module/catalog_for_main');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_tax'] = $this->language->get('text_tax');

        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');

        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        $data['categories'] = array();
        $filter_data = array();
        $сategories = array_slice($setting['category'], 0);
        $data_product = '';
        foreach ($сategories as $category_id) {
            $data_product['filter_category_id'] = '';
            $data_product['filter_sub_category'] = '';
            $category_info = $this->model_catalog_category->getCategory($category_id);
            $data_product['filter_category_id'] = (int)$category_info['category_id'];
            if ($data_product['filter_category_id'] !== '') {
                $products_category = $this->model_catalog_product->getProductsformain($data_product);
                $category_products = [];
            }
            $sub_categories = $this->model_catalog_category->getCategories($category_id);
            $subCategories = [];
            foreach ($sub_categories as $sub_category) {
                $data_product['filter_sub_category'] = '';
                $data_product['filter_sub_category'] = (int)$sub_category['category_id'];
                if ($data_product['filter_sub_category'] !== '') {
                    $products = $this->model_catalog_product->getProductsformain($data_product);
                    $subCategoryProducts = [];
                    foreach ($products as $product) {
                        if ($product) {
                            if ($product['image']) {
                                $image = $this->model_tool_image->$product['image'];
                            }
                            $subCategoryProducts[] = [
                                'product_id' => $product['product_id'],
                                'name' => $product['name'],
                                'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
                            ];
                        }
                    }
                }

                if ($sub_category) {
                    $subCategories[$sub_category['category_id']] = [
                        'subcategory_id' => $sub_category['category_id'],
                        'parent_id' => $sub_category['parent_id'],
                        'name' => $sub_category['name'],
                        'products' => $subCategoryProducts
                    ];
                }
            }

            foreach ($products_category as $product) {
                if ($product) {
                    if ($product['image']) {
                        $image = $this->model_tool_image->$product['image'];
                    }
                    $category_products[] = [
                        'product_id' => $product['product_id'],
                        'name' => $product['name'],
                        'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
                    ];
                }
            }

            $data['categories'][$category_id] = [
                'category_id' => $category_info['category_id'],
                'name' => $category_info['name'],
                'image' => $category_info['image'],
                'category_products' => $category_products,
                'sub_categories' => $subCategories
            ];
        }
       /*echo '<pre>';
        var_dump( $data['categories']);
       echo '</pre>';*/
        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/catalog_for_main.tpl')) {
            return $this->load->view($this->config->get('config_template') . '/template/module/catalog_for_main.tpl', $data);
        } else {
            return $this->load->view('EVO/template/module/category_for_main.tpl', $data);
        }


    }
}