<?php

class ControllerBlogBlogAjaxLoader extends Controller{
    public function index(){
        $this->load->model('blog/article');
        $this->load->language('blog/blog');

        $data['articles'] = array();

        $results = 'sad';

        foreach ($results as $result) {
            $data['articles'][] = array(
                'article_id' => $result['article_id'],
                'name' => $result['name'],
                'date_modified' => date($this->language->get('date_format_short'), strtotime($result['date_modified'])),
                'description' => html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'),
                'href' => $this->url->link('blog/article', 'article_id=' . $result['article_id'])
            );
        }

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/blog/blog_ajax_loader.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/blog/blog_ajax_loader.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/blog/blog_ajax_loader.tpl', $data));
        }
    }
}