<?php echo $header; ?>
<div class="row">
    <div id="content" class="<?php echo $class; ?>">
        <div class="col-lg-12 our_company_information">
            <div class="col-lg-4 no_padding">
                <div id="cont1" style="height: 322px; width: 300px;  float: left;">
                    <div id="features">
                        <div><div style="height: 322px; width: 300px; background: url('/catalog/view/theme/EVO/image/ourCompany/ph1.png') no-repeat center left;"></div></div>
                        <div><div style="height: 322px; width: 300px; background: url('/catalog/view/theme/EVO/image/ourCompany/ph2.png') no-repeat center left;"></div></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 lines_right border_line">

                <div class="contact_heading">
                    <div class="heading"><?php echo $heading_title; ?></div>
                    <div class="kaplij"></div>
                    <div class="theme_line"></div>
                </div>
                <?php echo $text_content; ?>

                    <div class="col-lg-4 no_padding">
                        <img src="/image/tl1.png">
                    </div>
                    <div class="col-lg-4 no_padding">
                        <img src="/image/tl2.png">
                    </div>
                    <div class="col-lg-4 no_padding">
                        <img src="/image/tl3.png">
                    </div>

            </div>

        </div>

        <div class="col-lg-12 news_products">
            <div class="col-lg-4 lines products_on_main_page">
                <?php echo $blog;?>
            </div>
            <div class="col-lg-8 products_on_main_page">
                <?php echo $content_bottom;?>
            </div>

        </div>
    </div>
    <?php echo $footer; ?>