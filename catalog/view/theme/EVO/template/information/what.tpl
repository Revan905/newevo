<?php echo $header; ?>
<div class="row">
    <div id="content">
        <div class="col-lg-12">
            <div class="contact_heading">
                <div class="heading"><?php echo $heading_title; ?></div>
                <div class="kaplij"></div>
                <div class="theme_line"></div>
            </div>
            <?php echo $text_content; ?>
            <div class="molecula"></div>
        </div>
    </div>
    <?php echo $content_bottom; ?>
    <?php echo $footer; ?>