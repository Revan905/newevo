<?php echo $header; ?>
<div class="row">
<div id="content" >
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal contact_form_main_block">
        <div class="col-lg-12">
            <div class="contact_heading">
                <div class="heading for_contact"><?php echo $heading_title; ?></div>
                <div class="kaplij"></div>
                <div class="theme_line"></div>
            </div>
        </div>
        <div class="col-lg-3 for_contact_divs">
            <div class="required">
                <label class="control-label contact_label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="name" value="<?php echo $name; ?>" id="input-name" class="contact_form"/>
                <?php if ($error_name) { ?>
                <div class="text-danger"><?php echo $error_name; ?></div>
                <?php } ?>
            </div>
            <div class="required">
                <label class=" control-label contact_label" for="input-email"><?php echo $entry_email; ?></label>
                <input type="text" name="email" value="<?php echo $email; ?>" id="input-email"
                       class=" contact_form"/>
                <?php if ($error_email) { ?>
                <div class="text-danger"><?php echo $error_email; ?></div>
                <?php } ?>
            </div>
            <div class="required">
                <label class="control-label contact_label" for="input-email"><?php echo $entry_theme; ?></label>
                <input type="text" name="theme" value="<?php echo $theme; ?>" id="input-theme"
                       class="contact_form"/>
                <?php if ($error_theme) { ?>
                <div class="text-danger"><?php echo $error_theme; ?></div>
                <?php } ?>
            </div>
        </div>
        <div class="col-lg-9 for_contact_divs">
            <div class="panel-body">
                <div class="row">
                    <div class="manufacturer">
                        <?php if ($image) { ?>
                        <div class="col-sm-3"><img src="<?php echo $image; ?>" alt="<?php echo $store; ?>"
                                                   title="<?php echo $store; ?>" class="img-thumbnail"/></div>
                        <?php } ?>
                        <?php echo $text_address; ?><br/>
                        <?php echo $text_telephone; ?>
                        <?php echo $telephone; ?><br/>
                        <?php echo $text_email; ?>
                        <a href="<?php echo 'mailto:'.$conf_email; ?>"><?php echo $conf_email; ?></a>
                        <br/>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="required">
                <label class="contact_label" for="input-enquiry"><?php echo $entry_enquiry; ?></label>
                <textarea name="enquiry" rows="10" id="input-enquiry"
                          class="contact_form"><?php echo $enquiry; ?></textarea>
                <?php if ($error_enquiry) { ?>
                <div class="text-danger"><?php echo $error_enquiry; ?></div>
                <?php } ?>

            </div>
        </div>
        <div class="buttons">
            <div class="pull-left">
                <div class="col-lg-12">
                    <input type="submit" value="<?php echo $text_submit; ?>"/>
                </div>
            </div>
        </div>
    </form>
    <div class="molecula"></div>
    </div>
    <?php echo $content_bottom; ?>
    <?php echo $footer; ?>

