<?php echo $header; ?>
<div class="row">
    <div id="content">
        <div class="col-lg-12 start-height">
            <div class="col-lg-4 lines start-height_child">
                <img class="img-responsive" src="/catalog/view/theme/EVO/image/pr4.png"/>
            </div>

            <!-- <div class="breadcrumb">
                 <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                 <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
                 <?php } ?>
             </div>-->
            <div class="col-lg-8 border_line start-height_child">
                <div class="contact_heading for_static">
                    <div class="heading"><b><?php echo $heading_title; ?></b></div>
                    <div class="kaplij"></div>
                    <div class="theme_line"></div>
                </div>

                <?php echo $text_content; ?>
                <div class="molecula"></div>
            </div>

        </div>
    </div>
    <?php echo $content_bottom; ?>
    <?php echo $footer; ?>