<?php echo $header; ?>

  <div class="row">
      <div class="col-lg-4 category_left_block">
          <?php echo $column_left; ?>
      </div>
  <div class="col-lg-8 lines_right category_products">
      <div class="contact_heading for_category">
          <div class="heading">
              <?php echo $heading_title; ?>
          </div>
          <div class="kaplij"></div>
          <div class="theme_line"> </div>
      </div>
       <?php foreach ($categories as $category) { ?>


      <div class="col-lg-12 subcategory_products">
          <p class="subcategory_catalog_heading"><?php echo $category['name']; ?> </p>
          <?php foreach ($category['category_products'] as $product) { ?>
            <div class="col-lg-4 products_on_main_page">
                <div class="product-thumb">
                    <div class="image">
                        <a href="<?php echo '?route=product/product&product_id='.$product['product_id']; ?>">
                            <img src="<?php echo 'image/'.$product['image']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo  $product['name']; ?>" class="img-responsive" />
                        </a>
                    </div>
              </div>
               <div class="caption">
                    <div class="product_text"><a href="<?php echo '?route=product/product&product_id='.$product['product_id']; ?>"><?php echo $product['name']; ?></a></div>
              </div>
            </div>

          <?php } ?>
          </div>
          <?php } ?>

         <?php if ($products) { ?>
            <?php foreach ($products as $product) { ?>
              <div class="col-lg-4 products_on_main_page">
                  <div class="product-thumb">
                      <div class="image">
                          <a href="<?php echo $product['href']; ?>">
                              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                          </a>
                      </div>
                  </div>
                  <div class="caption">
                      <div class="product_text"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
                  </div>
              </div>
        <?php } ?>

      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      </div>




<?php echo $footer; ?>
