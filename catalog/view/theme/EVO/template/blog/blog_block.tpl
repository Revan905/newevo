<div class="news_header"><strong><p><?php echo $name; ?></p></strong></div>
<?php if ($articles) { ?>
<div id="articles">
    <?php foreach ($articles as $article) { ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="article-layout article-list col-xs-12">
                <div class="article-intro">
                    <div>
                        <!--<h3><a href="<?php echo $article['href']; ?>"><?php echo $article['name']; ?></a></h3>-->
                        <p class="date_black"><?php echo $article['date_public']; ?></p>
                        <p><?php echo $article['description']; ?></p>
                    </div>
                </div>
                <!-- <div class="buttons">
                     <div class="pull-right"><a href="<?php echo $article['href']; ?>"
                                                class="btn btn-primary"><?php echo $button_read_more; ?></a></div>
                 </div>-->
            </div>

        </div>
    </div>
    <?php } ?>
</div>
<!--<div class="row">
    <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>

</div>-->
<?php } ?>
<div id="all_news_button" onclick="AjaxLoad()"></div>
<script>
    function AjaxLoad() {
        $.ajax({
            method: "POST",
            url: "index.php?route=blog/blog_block/articlesLoadAjax",
            data: {
                'limit': '<?php echo $article_total; ?>',
                'start': '<?php echo $limit++; ?>',
                'sort': '<?php echo $sort; ?>'
            },
        })
                .done(function (data) {
                    $.each(data.articles, function (key, value) {
                        $('#articles').append("<div class='panel panel-default'><div class='panel-body'> <div class='article-layout article-list col-xs-12'> <div class='article-intro'> <div> <p class='date_black'>" + value.date_public + "</p> <p>" + value.description + "</p> </div> </div> </div> </div>");
                    });
                    $('#articles').append("<div id='start_news_button' onclick='AjaxUnLoad()'></div>");
                       var height = $('#news').height();
                        $('.category_main_slim_padding').height(height);
                });
    }
    function AjaxUnLoad() {
        $.ajax({
            method: "POST",
            url: "index.php?route=blog/blog_block/articlesLoadAjax",
            data: {
                'limit': '<?php echo '2' ?>',
                'start': '<?php echo '0'; ?>',
                'sort': '<?php echo $sort; ?>'
            },
        })
                .done(function (data) {
                    $('#articles').empty();
                    $.each(data.articles, function (key, value) {
                        $('#articles').append("<div class='panel panel-default'><div class='panel-body'> <div class='article-layout article-list col-xs-12'> <div class='article-intro'> <div> <p class='date_black'>" + value.date_public + "</p> <p>" + value.description + "</p> </div> </div> </div> </div>");
                    });
                    $('#articles').append("<div id='all_news_button' onclick='AjaxLoad()'></div>");
                    var height = $('#news').height();
                    $('.category_main_slim_padding').height(height);
                });
    }
</script>
<?php if (!$articles) { ?>
<p><?php echo $text_empty; ?></p>
<?php } ?>