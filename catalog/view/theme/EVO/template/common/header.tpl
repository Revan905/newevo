<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8"/>

    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>"/>
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>"/>
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>"/>
    <?php } ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if ($icon) { ?>
    <link href="<?php echo $icon; ?>" rel="icon"/>
    <?php } ?>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>"/>
    <?php } ?>
    <script src="/catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="/catalog/view/theme/EVO/js/jquery-migrate.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="/catalog/view/theme/EVO/js/jquery.jshowoff.min.js"></script>
    <link href="/catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"/>
    <script src="/catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="/catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css"/>
    <link href="/catalog/view/theme/EVO/stylesheet/fonts.css" rel="stylesheet" type="text/css"/>
    <link href="/catalog/view/theme/EVO/stylesheet/style.css" rel="stylesheet">
    <script src="/catalog/view/theme/EVO/js/main.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('#features').jshowoff({
                speed: 15000,
                links: false,
                controlText:{play:'',pause:'',previous:'',next:''}
            });
            $('div.jshowoff-next').css('height','44px');
            $('div.jshowoff-next').css('width','20px');
            $('div.jshowoff-next').css('background', 'url("/catalog/view/theme/EVO/image/trik.png") no-repeat scroll center center');
            $('div.jshowoff-next').css('position','absolute');
            $('div.jshowoff-next').css('top',(($('div#cont1').height()/2)-20)+'px');
            $('div.jshowoff-next').css('left',($('div#cont1').width()-15)+'px');



        });
    </script>
    <?php foreach ($styles as $style) { ?>
    <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>"
          media="<?php echo $style['media']; ?>"/>
    <?php } ?>
    <script src="catalog/view/javascript/common.js" type="text/javascript"></script>
    <?php foreach ($scripts as $script) { ?>
    <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php echo $google_analytics; ?>
</head>
<body class="<?php echo $class; ?>">
<div class="container">
    <div class="row">
        <div class="col-lg-12 without_padding"><a href="/index.php"><div class="navigation_main"></div></a>
            <img class="img-responsive full_size" src="catalog/view/theme/EVO/image/evowide.png" alt="EVO""/>
            <div class='menu'>
                <ul class='dropdown'>
                    <li class='drop'>
                        <a href=""><?php echo $text_company; ?></a>
                        <ul class="list-group">
                            <li class="list-group-item"><a href="?route=information/our_company"><span class="menudesc"></span><?php echo $text_our_company; ?></a></li>
                            <li class="list-group-item"><a href="?route=information/production"><span class="menudesc"></span><?php echo $text_production; ?></a></li>
                            <li class="list-group-item"><a href="?route=information/innovations"><span class="menudesc"></span><?php echo $text_innovations; ?></a></li>
                        </ul>
                    </li>
                    <li class='drop'><a href="http://newevo.loc/index.php?route=product/category&path=65"><?php echo $text_products; ?></a></li>
                    <li class='drop'><a href=""><?php echo $text_oilselector; ?></a></li>
                    <li class='drop'><a href=""><?php echo $text_technlogies; ?></a>
                        <ul class="list-group">
                            <li class="list-group-item"><a href="?route=information/technology"><span class="menudesc"></span><?php echo $text_technology_iron_defence; ?></a></li>
                            <li class="list-group-item"><a href="?route=information/glossary"><span class="menudesc"></span><?php echo $text_glossary; ?></a></li>
                            <li class="list-group-item"><a href="?route=information/motor_industry"><span class="menudesc"></span><?php echo $text_motor_industry; ?></a></li>
                            <li class="list-group-item"><a href=""><span class="menudesc"></span><?php echo $text_motor_oil; ?></a>
                                <ul class="list-group">
                                    <li class="list-group-item"><a href="?route=information/what"><span class="menudesc"></span><?php echo $text_what_does_motor_oil_do; ?></a>
                                    </li>
                                    <li class="list-group-item"><a href="?route=information/how"><span class="menudesc"></span><?php echo $text_how_to_read_oil_can; ?></a>
                                    </li>
                                </ul>
                            </li>
                            <li class="list-group-item"><a href="?route=information/approvals"><span class="menudesc"></span><?php echo $text_approvals; ?></a></li>
                            <li class="list-group-item"><a href=""><span class="menudesc"></span><?php echo $text_optimal_oil_drain_intervals; ?></a>
                                <ul class="list-group">
                                    <li class="list-group-item"><a href="?route=information/engine_oil"><span class="menudesc"></span><?php echo $text_engine_oil_change_interval; ?></a>
                                    </li>
                                    <li class="list-group-item"><a href="?route=information/gear_oil"><span class="menudesc"></span><?php echo $text_gear_oil_change_interval; ?></a>
                                    </li>
                                </ul>
                            </li>
                            <li class="list-group-item"><a href="?route=information/useful_links"><span class="menudesc"></span><?php echo $text_useful_links; ?></a>
                            </li>
                        </ul>
                    </li>
                    <li class='drop'><a href="?route=information/contact">Contact</a></li>
                </ul>
                <?php echo $language; ?>
            </div>
        </div>
    </div>




