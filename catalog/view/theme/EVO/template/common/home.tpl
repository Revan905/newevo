<?php echo $header; ?>
  <div class="row ">
    <div id="content" class="<?php echo $class; ?>">
        <?php echo $content_top;?>
        <div class="col-lg-12 news_products">
            <div class="col-lg-4 lines products_on_main_page no_padding_left" id="news">
                <?php echo $blog;?>
            </div>
            <div class="col-lg-8 products_on_main_page">
                <?php echo $content_bottom; ?>
            </div>
        </div>
    </div>
<?php echo $footer; ?>