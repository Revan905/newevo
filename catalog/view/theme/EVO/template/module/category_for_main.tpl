<div class="row products_on_main_page" style="display: flex;">
    <?php $left_coloumn = array_slice($categories, 0, round(count($categories)/2, 0, PHP_ROUND_HALF_DOWN)); ?>
    <?php $right_coloumn = array_slice($categories, round(count($categories)/2, 0, PHP_ROUND_HALF_DOWN), count($categories)); ?>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 lines products_on_main_page category_main_slim_padding">
         <?php  foreach ($left_coloumn as  $category) {  ?>
            <p class="products_on_main_for_categories">
                <img class="category_image" src="<?php echo '/image/'.$category['image'];?>">
                <strong><?php echo $category['name']; ?></strong>
             </p>
        <?php if (!empty($category['category_products'])) { ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <ul class="main_products">
                    <?php foreach ($category['category_products'] as $product) { ?>
                    <li>
                        <a class="links" href="<?php echo $product['href']; ?>">
                            <?php echo $product['name']; ?>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
            <?php // var_dump($category['category_products']); ?>
            <?php if(isset($category['sub_categories'])) { ?>
            <?php foreach ($category['sub_categories'] as $sub_category) { ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p class="subcategory">
                        <span class="bubble"></span>
                        <strong><?php echo $sub_category['name'];?></strong>
                    </p>
                    <ul class="main_products">
                        <?php foreach ($sub_category['products'] as $product) { ?>
                        <li>
                            <a class="links" href="<?php echo $product['href']; ?>">
                                <?php echo $product['name']; ?>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <?php } ?>
         <?php } ?>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 lines products_on_main_page category_main_slim_padding">
        <?php  foreach ($right_coloumn as  $category) {  ?>
            <p class="products_on_main_for_categories">
                <img class="category_image" src="<?php echo '/image/'.$category['image'];?>">
                <strong><?php echo $category['name']; ?></strong>
            </p>
        <?php if (!empty($category['category_products'])) { ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="main_products">
                <?php foreach ($category['category_products'] as $product) { ?>
                <li>
                    <a class="links" href="<?php echo $product['href']; ?>">
                        <?php echo $product['name']; ?>
                    </a>
                </li>
                <?php } ?>
            </ul>
        </div>
        <?php } ?>
            <?php foreach ($category['sub_categories'] as $sub_category) { ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p class="subcategory">
                        <span class="bubble"></span>
                        <strong><?php echo $sub_category['name'];?></strong>
                    </p>
                    <ul class="main_products">
                        <?php foreach ($sub_category['products'] as $product) { ?>
                        <li>
                            <a class="links" href="<?php echo $product['href']; ?>">
                                <?php echo $product['name']; ?>
                            </a>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
        <?php } ?>
        <div class="molecula"></div>
        </div>
    </div>
