<?php foreach ($categories as $category) { ?>

        <?php if ($category['category_id'] == $category_id) { ?>
            <div class="category">
             <a class="category_super_category" href="<?php echo $category['href']; ?>" >
                 <div class="block_for_category_image"><img src="<?php echo 'image/'.$category['image']; ?>" class="category_img"/></div>
                 <?php echo $category['name']; ?>
             </a>
              <?php if ($category['children']) { ?>
                  <?php foreach ($category['children'] as $child) { ?>
                      <?php if ($child['category_id'] == $child_id) { ?>
                          <div class="sub_category">
                              <a class="sub_category_category" href="<?php echo $child['href']; ?>" ><?php echo $child['name']; ?></a>
                          </div>
                      <?php } else { ?>
                      <div class="sub_category">
                          <a class="sub_category_category" href="<?php echo $child['href']; ?>" ><?php echo $child['name']; ?></a>
                      </div>
                      <?php } ?>
                  <?php } ?>
              <?php } ?>
            </div>
            <?php } else { ?>
              <div class="category">
                 <a class="category_super_category" href="<?php echo $category['href']; ?>" >
                     <div class="block_for_category_image">
                         <?php if($category['image']){ ?><img src="<?php  echo 'image/'.$category['image'];  ?>"  class="category_img"/><?php } ?>
                        </div>
                     <?php echo $category['name']; ?>
                 </a>
              </div>
                <?php } ?>
        <?php } ?>


