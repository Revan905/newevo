<?php
// Text
$_['text_home']          = 'Home';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_company']       = 'Company';
$_['text_products']      = 'Products';
$_['text_oilselector']   = 'Oil Selector';
$_['text_technlogies']   = 'Technlogies';
$_['text_all']           = 'See All';
$_['text_our_company']   = 'Our company';
$_['text_production']    = 'Production';
$_['text_innovations']   = 'Innovations';
$_['text_technology_iron_defence']   = 'Technology Iron Defence';
$_['text_glossary']   = 'Glossary';
$_['text_motor_industry']   = 'Motor industry';
$_['text_motor_oil']   = 'Motor oil';
$_['text_approvals']   = 'Approvals';
$_['text_optimal_oil_drain_intervals']   = 'Optimal oil drain intervals';
$_['text_useful_links']   = 'Useful links';
$_['text_what_does_motor_oil_do']   = 'What does motor oil do?';
$_['text_how_to_read_oil_can']   = 'How to read oil can?';
$_['text_engine_oil_change_interval']   = 'Engine oil change interval';
$_['text_gear_oil_change_interval']   = 'Gear oil change interval';

