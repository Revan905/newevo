<?php
$_['heading_title']    = 'GLOSSARY';
$_['text_content']     = '<p class="pp2"><strong>ACEA</strong> - European Automobile Manufacturers Association (French: Association des Constructeurs Européens d\'Automobiles).</p>
<p class="pp2"> <strong>Additive</strong> - A chemical compound or compounds added to a lubricant for the purpose of imparting new properties or to improve those properties which the lubricant already has.</p>
<p class="pp2"><strong>API</strong> - American Petroleum Institute</p>
<p class="pp2"><strong>Ash Content</strong> - The percent by weight of residue left after the combustion of an oil sample (ASTM Method D482).</p>
<p class="pp2"><strong>ASTM</strong> - American Society for Testing Materials, a society for developing standards for materials and test methods.</p>
<p class="pp2"><strong>Base Stock</strong> - A fully refined lube oil, which is a component of lubricant formulations. Group I base stocks contain less than 90 percent saturates and/or greater than 0.03 percent sulfur and have a viscosity index greater than ore equal to 80 and less than 120. Group II base stocks contain greater than or equal to 90 percent saturates and less than or equal to 0.03 percent sulfur and have a viscosity index greater than or equal to 80 and less than 120. Group III base stocks contain greater than or equal to 90 percent saturates and less than or equal to 0.03 percent sulfur and have a viscosity index greater than or equal to 120. Group IV base stocks are polyalphaolefins (PAO). Group V base stocks include all other base stocks not included in Group I, II, III, or IV.</p>
<p class="pp2"><strong>Boiling Point</strong> - The temperature at which a substance boils, or is converted into vapor by bubbles forming with the liquid; it varies with pressure.</p>
<p class="pp2"><strong>Centipoise (cP.)</strong> - A unit of absolute viscosity. 1 centipoise = 0.01 poise.</p>
<p class="pp2"><strong>Centistoke (cSt.)</strong> - A standard unit of kinematic viscosity = 0.01 stoke.</p>
<p class="pp2"><strong>Diester Oil</strong> - A synthetic lubricating liquid made from esters; also called ester oil.</p>
<p class="pp2"><strong>DIN </strong> - German Institute for Standardization.</p>
<p class="pp2"><strong>Dispersing</strong> - in lubrication, usually used interchangeably with detergent. An additive that keeps fine particles of insoluble materials in a homogeneous solution. Hence, particles are not permitted to settle out and accumulate.</p>
<p class="pp2"><strong>EOLCS</strong> - API’s Engine Oil Licensing and Certification System.</p>
<p class="pp2"><strong>Flash Point </strong> - The lowest temperature at which vapors arising from the oil will ignite momentarily (i.e., flash) when exposed to a flame.</p>
<p class="pp2"><strong>Foam</strong> - An agglomeration of gas bubbles separated from each other by a thin liquid film that is observed as a persistent phenomenon on the surface of a liquid. It most often occurs by whipping air into a lubricant.</p>
<p class="pp2"><strong>Four-Ball Tester</strong> - This name is frequently used to describe either of two similar laboratory machines, the Four-Ball Wear Tester and the Four-Ball EP Tester. These machines are used to evaluate a lubricant\'s anti- wear qualities, frictional characteristics or load carrying capabilities.</p>
<p class="pp2"><strong>ILSAC</strong> - International Lubricants Standardization and Approval Committee.</p>
<p class="pp2"><strong>JAMA</strong> -  Japanese Automobile Manufacturers Association.</p>
<p class="pp2"><strong>JASO</strong> -  Japanese Automobile Standards Organization.</p>
<p class="pp2"><strong>JSAE</strong> - Society of Automotive Engineers, Japan.</p>
<p class="pp2"><strong>Kinematic Viscosity</strong> - The absolute viscosity of a fluid divided by its density. In a c.g.s. system, the standard unit of kinematic viscosity is the stoke and is expressed in sq. cm. per. sec. In the English system, the standard unit of kinematic viscosity is the newt and is expressed in sq. in. per sec.</p>
<p class="pp2"><strong>Lacquer </strong> - A deposit resulting from the oxidation and polymerization of fuels and lubricants when exposed to high temperatures. Similar to but harder than varnish.</p>
<p class="pp2"><strong>Multigrade Oil</strong> - An oil that meets the low temperature viscosity limits of one of the SAE W numbers as well as the 100°C viscosity limits of one of the non-W numbers.</p>
<p class="pp2"><strong>NLGI</strong> - National Lubricating Grease Institute.</p>
<p class="pp2"><strong>OEM</strong> - Original Equipment Manufacturer.</p>
<p class="pp2"><strong>Oxidation Stability</strong> - Ability of a lubricant to resist natural degradation upon contact with oxygen.</p>
<p class="pp2"><strong>Pour Point</strong> - The pour point of a lubricant is the lowest temperature at which the lubricant will pour or flow when it is chilled without disturbance under specified conditions.</p>
<p class="pp2"><strong>SAE</strong> - Society of Automotive Engineers.</p>
<p class="pp2"><strong>SAE Viscosity Number</strong> - System for classifying crankcase, transmission, and differential lubricants, according to their viscosities, established by the Society of Automotive Engineers. SAE numbers are used in connection with recommendations for crankcase oils to meet various design, service and temperature requirements affecting viscosity only; they do not denote quality.</p>
<p class="pp2"><strong>Sludge</strong> - Insoluble material formed as a result either of deterioration reactions in an oil or by contamination of an oil, or both.</p>
<p class="pp2"><strong>Total Base Number (TBN)</strong> - The quantity of base, expressed in terms of the equivalent number of milligrams of potassium hydroxide, that is required to titrate the strong base constituents present in 1g of sample (ASTM Method D 664 or D 974).</p>
<p class="pp2"><strong>Viscosity</strong> - hat property of a fluid or semi-solid substance characterized by resistance to flow and defined as the ratio of the shear stress to the rate of shear of a fluid element. The standard unit of viscosity in the c.g.s. system is the poise and is expressed in dyne sec. per square centimeter. The standard unit of viscosity in the English system is the reyn and is expressed in lb. sec. per square in.</p>
<p class="pp2"><strong>Viscosity Index (VI)</strong> - A measure of a fluid\'s change of viscosity with temperature. The higher the viscosity index the smaller the change in viscosity with temperature.</p>
';
