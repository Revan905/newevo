<?php
$_['heading_title'] = 'MOTOR INDUSTRY';
$_['text_content'] = '
<p class="pp2"><strong>History of the Internal Combustion Engine - The Heart of the Automobile</strong></p>
<p class="pp1">By definition an automobile or car is a wheeled vehicle that carries its own motor and transports passengers. The history of the automobile reflects a revolution that took place worldwide.</p>
<p class="pp2">An internal combustion engine is any engine that uses the explosive combustion of fuel to push a piston within a cylinder - the piston\'s movement turns a crankshaft that then turns the car wheels via a chain or a drive shaft.</p>
<p class="pp2">A brief outline of the history of the internal combustion engine includes the following highlights:</p>
<ul>
    <li><p class="pp2"><strong>1858</strong> - Belgian-born engineer, Jean Joseph Etienne Lenoir invented and patented (1860) a double-acting, electric spark-ignition internal combustion engine fueled by coal gas.</p></li>
    <li><p class="pp2"><strong>1864</strong> - Austrian engineer, Siegfried Marcus, built a one-cylinder engine with a crude carburetor, and attached his engine to a cart for a rocky 500-foot drive.</p></li>
    <li><p class="pp2"><strong>1866</strong> - German engineers, Eugen Langen and Nikolaus August Otto improved on Lenoir\'s and de Rochas\' designs and invented a more efficient gas engine.</p></li>
    <li><p class="pp2"><strong>1876</strong> - The first successful two-stroke engine was invented by Sir Dougald Clerk.</p></li>
    <li><p class="pp2"><strong>1885</strong> - Gottlieb Daimler invented the prototype of the modern gas engine - with a vertical cylinder, and with gasoline injected through a carburetor (patented in 1887). Daimler first built a two-wheeled vehicle the "Reitwagen" with this engine and a year later built the world\'s first four-wheeled motor vehicle.</p></li>
    <li><p class="pp2"><strong>1886</strong> - On January 29, Karl Benz received the first patent (DRP No. 37435) for a gas-fueled car.</p></li>
    <li><p class="pp2"><strong>1889</strong> - Daimler built an improved four-stroke engine with mushroom-shaped valves and two V-slant cylinders.</p></li>
    <li><p class="pp2"><strong>1890</strong> - Wilhelm Maybach built the first four-cylinder, four-stroke engine.</p></li>
    <li><p class="pp2"><strong>1913</strong> - Henry Ford installed the first conveyor for the mass production of internal combustion engines.</p></li>
    <li><p class="pp2"><strong>1920</strong> - Half of all the motor vehicles in the world are Model T Fords.</p></li>
    <li><p class="pp2"><strong>1921</strong> - Citroen was the first European manufacturer to adopt production-line manufacturing.</p></li>
    <li><p class="pp2"><strong>1926</strong> - Power steering system is intriduced.</p></li>
    <li><p class="pp2"><strong>1928</strong> - First strech limos are made.</p></li>
    <li><p class="pp2"><strong>1931</strong> - Mercedez-Benz make independent front suspension.</p></li>
    <li><p class="pp2"><strong>1939</strong> - First air conditioned cars are made.</p></li>
    <li><p class="pp2"><strong>1951</strong> - Airbag is invented.</p></li>
    <li><p class="pp2"><strong>1965</strong> - Emissions regulations introduced. Safety devices also became mandatory – before this, manufacturers only included seat belts as optional extras.</p></li>
    <li><p class="pp2"><strong>1978</strong> - Safe stopping distance decreased. The first antilock braking systems (ABS) were developed for automobiles by German manufacurers.</p></li>
    <li><p class="pp2"><strong>1992</strong> - Alternate fuel vehicles are encouraged.</p></li>
    <li><p class="pp2"><strong>1997</strong> - Car manufacturers go green.</p></li>
    <li><p class="pp2"><strong>2008</strong> - Car manufacturing companies started making moves to produce vehicles with higher fuel efficiency.</p></li>
    <li><p class="pp2"><strong>2010</strong> - "Easy Open" system was developed.</p></li>
    <li><p class="pp2"><strong>2010</strong> - First blind-spot monitor was introduced.</p></li>
    <li><p class="pp2"><strong>2011</strong> - Car manufacturers are in search of hybrid mixed energy sources to reduce the consequences of emissions into the atmosphere.</p></li>
    <li><p class="pp2"><strong>2012</strong> - "Active City Stop" system in the Ford Focus is the first assistance system, which supports driver even at speeds up to 30 km/h.</p></li>
    <li><p class="pp2"><strong>In May 2014</strong> - Google announced that 100 test vehicles will be built to be dispensed without the steering wheel, brake and accelerator. </p></li>
    <li><p class="pp2">Other development areas are the pedestrian protection, the use of recyclable raw materials as well as driverless transportation systems. General Motors plans to present first unmanned test car in 2015 and in mass production from 2018th.</p></li>
</ul>
';
