<?php
$_['heading_title'] = 'GEAR OIL CHANGE INTERVAL';
$_['text_content'] = '
<p class="pp1"><strong>Manual Transmission</strong></p>
<p class="pp1">Manual transmissions are lubricated with gear oil or engine oil in some cars, which must be changed periodically in some cars, although not as frequently as the automatic transmission fluid in a vehicle so equipped.</p>
<p class="pp1">Problem cause by fail of gear oil:</p>
<ul class="spsk1">
    <li>Gear-shifting will be more difficult</li>
    <li>Abnormal vibration from gearbox</li>
    <li>Gearbox damage.</li>
</ul>
<p class="pp1">Recommended interval:</p>
<p class="pp1">Every 60,000km or 3 years (which ever comes first)</p>
<p class="pp1"><strong>Differential Gear (Rear wheel drive & 4 wheels drive)</strong></p>
<p class="pp1">The differential in an automobile is a gear box that essentially allows different wheels to turn at various rates. Because the differential contains a variety of moving parts in the form of gears, it is essential to properly lubricate the parts to allow proper function and prevent damage. Differential oil is specifically designed for this purpose.</p>
<p class="pp1">Problem cause by fail of gear oil:)</p>
<ul class="spsk1">
    <li>Abnormal vibration from differential gearbox</li>
    <li>Both wheels are easy to be lock</li>
    <li>Differential gearbox damage.</li>
</ul>
<p class="pp1">Recommended interval: Every 40,000km or 2 years (which ever comes first)</p>
<p class="pp1"><strong>Automatic Transmission</strong></p>
<p class="pp1">Automatic transmission fluid (ATF) is the fluid used in vehicles with a self shifting or automatic transmission. It is typically colored red or green to distinguish it from motor oil and other fluids in the vehicle. On most vehicles its level is checked with a dipstick while the engine is running.</p>
<p class="pp1">The fluid is a highly refined mineral oil optimized for the special requirements of an automatic transmission, such as valve operation, brake band friction and the torque converter as well as gear lubrication. Synthetic ATF has also recently become available, offering better performance and service life for certain applications (such as frequent trailer towing).</p>
<p class="pp1">ATF is also used as a hydraulic medium in some power assisted steering systems and as a lubricant in some 4WD transfer cases. Problem cause by fail of gear oil:</p>
<ul class="spsk1">
    <li>Auto-shifting will be more difficult</li>
    <li>Gear selection may not work properly</li>
    <li>“Kick-down” function may not work properly.</li>
    <li>Gearbox damage.</li>
</ul>
<p class="pp1">Recommended interval: Every 20,000km or 1 year (which ever comes first)</p>
<p class="pp1"><strong>Continuous Variable Transmission (CVT)</strong></p>
<p class="pp1">Continuous Variable Transmission Fluid (CVTF) typically contains a wide variety of chemical compounds intended to provide the required properties of a particular CVTF specification. Most CVTF contain some combination of rust preventatives, anti-foam additives, detergents, dispersants, anti-wear additives, anti-oxidation compounds, surfactants, cold-flow improvers, high-temperature thickeners, gasket conditioners, and petroleum dye.)</p>
<p class="pp1">Some of the car makers may use the ATF as their CVT gearbox lubricant. While others car makers have their owned CVTF only for the CVT gearbox.
Problem cause by fail of gear oil:</p>
<ul class="spsk1">
    <li>Auto-shift will be more difficult</li>
    <li>Abnormal vibration from gearbox</li>
    <li>“Gear selection may not work properly</li>
    <li>“Kick-down” function may not work properly</li>
    <li>Gearbox damage.</li>
</ul>
<p class="pp1">Recommended interval:</p>
<ul class="spsk1">
    <li>Every 10,000km or 6 months (which ever comes first)</li>
    <li>Every 20,000km or 1 year (which ever comes first)</li>
</ul>
<p class="pp1"><strong>Direct-shift gearbox (DSG)</strong></p>
<p class="pp1">DSG gearbox work more efficient then manual transmission, but also has smoother gear-shift compare with automatic transmission and CVT gearbox. DSG gearbox has wet clutch design. DSG gear oil change interval is not often as automatic transmission and CVT gearbox.</p>
<p class="pp1">Problem cause by fail of gear oil:</p>
<ul class="spsk1">
    <li>(So far, there is no any fail gear oil record, except fail sensor)</li>
</ul>
<p class="pp1">Recommended interval: Every 40,000km or 2 years (which ever comes first)</p>
';
