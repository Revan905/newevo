<?php
$_['heading_title'] = 'HOW TO READ OIL CAN?';
$_['text_content'] = '
<p class="pp2">Just like nutrition facts printed on the label of every item in a grocery store, the symbols on a can of motor oil tell consumers the service rating and certification for that product.</p>
<p class="pp2">In the United States, the American Petroleum Institute (API) administers the licensing and certification of engine oils through a system that meets the warranty, maintenance and lubrication requirements of original equipment manufacturers. OEMs, oil marketers, additive companies and testing laboratories work together to establish engine oil performance requirements, test methods and limits for the various classifications and testing processes</p>
<p class="pp2">The system includes a formal licensing agreement executed by lubricant suppliers with API. Through this program, API has standardized the labeling of engine oils by adopting the donut logo. The logo was designed to be placed in a prominent position on a variety of lubricant quart/liter containers. API has also developed a starburst certification mark to select engine oils that meet the gasoline performance standards established by the International Lubricant Standardization and Approval Committee (ILSAC). This logo is displayed on the front of licensed motor oil product packages..</p>
<p class="pp2">To protect the consumer, API requires that all lubricant suppliers using the API Service Symbol obtain a license to use the symbol and sign an affidavit stating that test data is available to support performance claims.</p>
<p class="pp2">The first letter is always an S or C:</p>
<ul>
<li><p class="pp2">API S - Stands for gasoline engines (service classes and Spark Ignition)</p> </li>
<li><p class="pp2">API C - Stands for diesel engines (commercial classes also Compression Ignition)</p> </li>
</ul>
<p class="pp2">The second letter stands for the class (quality). The better quality is determined by the higher letter in the alphabet. The highest current API specifications are SN / CJ-4.</p>
<p class="pp2">The specification 5W-30 on the tin indicates the SAE class. It describes the viscosity (- is the measure of the flow properties of the oil). The letter "W" describes the flow properties of oil in the winter or cold weather. The number without "W" describes the flow behavior at a temperature of 100 degrees.</p>
<p class="pp2">The “Resource Conserving” supplemental category requires further properties. The former supplemental category, which was called “Energy Conserving” required only fuel saving properties from the oil. Resource Conserving requires further properties like: emission system protection, turbocharger protection, compatibility with engines operating on ethanol containing fuels, up to E-85.</p>
';
