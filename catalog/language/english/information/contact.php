<?php
// Heading
$_['heading_title']  = 'Contact';

// Text
$_['text_location']  = 'Our Location';
$_['text_store']     = 'Our Stores';
$_['text_contact']   = 'Contact Form';
$_['text_address']   = 'Manufacturer: EVO Lubricants GmbH<br/>Keithstr. 2-4<br/>10787 Berlin <br/>Germany';
$_['text_telephone'] = 'Tel:';
$_['text_email'] = 'Email:';

$_['text_fax']       = 'Fax';
$_['text_open']      = 'Opening Times';
$_['text_comment']   = 'Comments';
$_['text_success']   = '<p>Your enquiry has been successfully sent to the store owner!</p>';
$_['text_submit']		= 'Send';

// Entry
$_['entry_name']     = 'Enter your Name:';
$_['entry_email']    = 'E-mail address:';
$_['entry_theme']    = 'Message Subject:';
$_['entry_enquiry']  = 'Enter your Message:';
$_['entry_captcha']  = 'Enter the code in the box below';

// Email
$_['email_subject']  = 'Enquiry %s';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';
$_['error_captcha']  = 'Verification code does not match the image!';
