<?php
$_['heading_title'] = 'ENGINE OIL CHANGE INTERVAL';
$_['text_content'] = '
<p class="pp2">On most new vehicles, the factory recommended interval for changing the engine oil and oil filter is typically <strong>3 months</strong> or every <strong>5,000km</strong> in passenger car and light truck gasoline engines. Some OEMs even recommend 10,000 km oil change intervals. A growing number of OEMs make no specific mileage or time recommendation and use oil monitoring software to turn on an oil change reminder light when the oil is estimated to need changing. For diesel engines and turbocharged gasoline engines, the recommended interval is typically every <strong>3,000km</strong> or <strong>2 months</strong>.</p>
<p class="pp2">If you read the fine print in your owner’s manual on vehicles where a specific mileage/time recommendation is made, you’ll often discover that the every 6 months or 10,000km oil change is for vehicles that are driven under <strong>“ideal”</strong> operating circumstances. What most of people think of as <strong>“normal”</strong> driving is actually <strong>“severe service”</strong> driving.</p>
<p class="pp2">Severe service driving includes:</p>
<ul class="spsk1">
<li>Frequent short trips (less than 20km, especially during cold weather)</li>
<li>Stop-and-go city traffic driving</li>
<li>Driving in dusty conditions (gravel roads, etc.)</li>
<li>Driving at sustained highway speeds during hot weather</li>
</ul>
<p class="pp2">For severe service driving, the most common recommendation is to change the oil every <strong>5,000km</strong> or <strong>3 months</strong> (which ever comes first). This is especially important on older, high mileage (over 160,000km) engines.</p>
<p class="pp2">For maximum protection, many people change their oil every <strong>5,000km</strong> or <strong>3 months</strong> regardless of what kind of driving they do. But some would say this is excessive maintenance. Changing the oil every 5,000km is probably not necessary on a newer vehicle with a low mileage engine, especially if it is driven more than 20km one-way daily or is used primarily for highway driving or long distance commuting.</p>
<p class="pp2">A newer engine with little or no wear can probably go 10,000km to 15,000km between oil changes with no harm. However, pushing the oil change interval beyond 10,000km can cause troubles, especially if an engine has more than 125,000km on it, or it operated under <strong>“severe”</strong> conditions as described above.</p>
';
