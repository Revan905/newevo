<?php
$_['heading_title'] = 'USEFUL LINKS';
$_['text_content'] = '
<p class="pp1"><a class="links" target="_blank" href="http://www.api.org">American Petroleum Institute (API)</a></p>
<p class="pp1"><a class="links" target="_blank" href="http://www.sae.org">Society of Automotive Engineers (SAE)</a></p>
<p class="pp1"><a class="links" target="_blank" href="http://www.astm.org">American Society for Testing and Materials (ASTM)</a></p>
<p class="pp1"><a class="links" target="_blank" href="http://www.jsae.or.jp">Society of Automotive Engineers of Japan, Inc. (JSAE)</a></p>
<p class="pp1"><a class="links" target="_blank" href="http://www.oilspecifications.org">Oilspecification</a></p>
<p class="pp1"><a class="links" target="_blank" href="http://www.carbibles.com">Carbibles</a></p>
<p class="pp1"><a class="links" target="_blank" href="http://www.nitrobahn.com">Nitrobahn</a></p>
<p class="pp1"><a class="links" target="_blank" href="http://www.tribology-abc.com">Engineering-ABC</a></p>
<p class="pp1"><a class="links" target="_blank" href="http://www.machinerylubrication.com">Machinery lubrication</a></p>

';
