<?php
$_['heading_title'] = 'MOTORÖLWECHSELINTERVALL';
$_['text_content'] = '
<p class="pp2">Ein Ölwechsel verlängert die Lebenszeit Ihres Autos.</p>
<p class="pp2">Kaum etwas strapaziert Ihren Motor so wie die Verwendung von altem oder zu wenig Motoröl: Die dadurch entstehende erhöhte Reibung im Motorinnern führt zu mehr Verschleiß, höherem Spritverbrauch und mehr Abgasemission. </p>
<p class="pp2">Was bei uns zum Ölwechsel dazu gehört:</p>
<ul>
    <li>Altöl ablassen</li>
    <li>Ölfilter erneuern</li>
    <li>Dichtungsringe tauschen</li>
    <li>Frisches Öl auffüllen</li>
    <li>Altöl umweltgerecht entsorgen</li>
    <li>Ölwechselintervallanzeige zurückstellen</li>
</ul>
<p class="pp2">In der Regel ist ein Ölwechsel viermal pro Jahr aus Expertensicht Pflicht. Fahren Sie sehr viel, empfiehlt es sich das Öl häufiger zu wechseln, jeweils nach etwa 5 000 km. Beim Einsatz von Longlife-Ölen genügt ein Ölwechsel alle 10 000km oder alle 2 Jahre.</p>
<p class="pp2">Die von den Fahrzeugherstellern empfohlenen Ölwechselintervalle sind daher unbedingt einzuhalten. Sie werden in Abhängigkeit von der Konstruktion des Motors und der Qualität des vorgeschriebenen Motoröls festgelegt.</p>
<p class="pp2">Vorschriftsmäßige Einhaltung der Intervalle verringert die Lebensdauerkosten (Reparaturen, Sprit, Wertverlust) um drei Viertel.</p>
<p class="pp2">Bei besonders belastenden Betriebsbedingungen sollte das Intervall verkürzt werden. Hierzu zählen:</p>
<ul>
    <li>Stop and Go Verkehr in der Stadt</li>
    <li>Häufige Kaltstarts</li>
    <li>Volllastbetrieb, Verwendung von Anhängern </li>
    <li>Besonders heißes Klima</li>
</ul>
';
