<?php
$_['heading_title']    = 'UNSERE FIRMA';
$_['text_content']     = '<p class="pp1">Die vervollkommnete auf der neuen Technologie des Motorbestandteileschutzes auf dem Molekülniveau basierende Formel des EVO ORIGINAL Öls wurde in der Zusammenarbeit mit den Nanotechnologen ausgearbeitet.</p>
<p class="pp1">Die Molekülen Iron Defence, beim Kontakt mit den Bauteilflächen, dringen in die Oberfläche der Reibung und bilden die sicherste Ölfilm mit hohen Antifriktionseigenschaften, die vor dem Verschleiß schützt und wesentlich die Lebensdauer des Motors verlängert.</p>
<p class="pp1">Die Molekülen Iron Defence arbeiten immer - vom Motorstart und in allen Betriebsweisen und konzentrieren sich dabei auf solche Bauelemente, die in den temperatur- und mechanischen Höchstbelastungen arbeiten.</p>
<p class="pp1">Das professionell ausgearbeitete Additivpaket verhindert den Rußansatz, Harz- und Lackablagerung auf den Innenflächen des Motors, garantiert eine stabile Viskosität unter allen Betriebs- und Klimaumständen und gewährleistet blitzschnelle Ölförderung beim Start und auch bei den niedrigen Temperaturen.</p>';
