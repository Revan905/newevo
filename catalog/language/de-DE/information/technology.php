<?php
$_['heading_title']    = 'TECHNOLOGIE DES EISENSCHUTZES';
$_['text_content']     = '<p class="pp1">In EVO Lubricants GmbH haben wir die Spezialisten der Brennstoff-, Chemie- und Maschinenbauindustrie versammelt, um die Schmiermaterialien, die die Aufforderungen der Verkehrsmittelhersteller befriedigen, herzustellen. Dank der gemeinsamen Bemühungen hat die EVO Lubricants GmbH eine einzigartige Schmierproduktion, die den hohen Aufforderungen der Verkehrsmittel- und Motorenhersteller entspricht, hergestellt.</p>
<p class="pp1"><strong>Unsere Philosophie</strong></p><br/>
<p class="pp1">Wir sind der Meinung, dass das Niveau der technologischen und wissenschaftlichen Ausarbeitung im 21. Jahrhundert erlaubt nicht, einen Produkt als ein Endresultat zu betrachten.</p><br/>
<p class="pp1">Das Produkt ist ein Prozess. Im Falle des Motoröls ist dieser Prozess mit der Entwicklung des Maschinenbaus verbunden. Der gegenwärtige Motor ist ein Ergebnis der mehrjährigen Arbeit, wo die Vervollkommnung der alten Konstruktionen mit völlig neuen Modellen aufgrund der Veränderung der Effektivitätsgrundlagen verbunden sind.</p>
<p class="pp1">Die Evolution der Hauptkomponenten eines Autos hat eine große Verantwortung auf die Ölhersteller gelegt, um die Aufforderungen der modernen Motortechnologien zu berücksichtigen.</p>';
