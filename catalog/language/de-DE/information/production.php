<?php
$_['heading_title']    = 'HERSTELLUNG';
$_['text_content']     = '<p class="pp1">Unser Betrieb ist am modernsten ausgerüstet. Wir garantieren eine ausführliche Qualitätskontrolle des ganzen Zyklus der Ölherstellung, von dem Rohstoff bis zur Lieferung der verpackten Kanister. Alle Herstellungsetappen sind mit einem modernen Labor für die Bewahrung der Ölmuster und EVO-Qualitätskontrolle ausgestattet. Jedes Produkt wird mehrmals getestet, wobei auch die Qualität unter den realen Betriebsbedingungen geprüft wird. Wir haben alle mögliche Risikofaktoren ausgeschlossen, um das beste Öl für Ihren Motor zu bekommen.</p>
<br/><br/><br/><br/><br/><br/><br/>';

