<?php
$_['heading_title'] = 'GETRIEBEÖLWECHSELINTERVALL';
$_['text_content'] = '
<p class="pp1"><strong>Getriebeölwechselintervall</strong></p>
<p class="pp1">Das Getriebeöl schmiert die beweglichen Teile im Getriebe und vermindert dadurch Reibung. Durch zu wenig oder zu viel Öl können bei AT Fahrzeugen Wandlerschäden entstehen und bei Mechanischen Getrieben unter anderem Ausrücklagerschäden oder sogar Schäden an den Getriebewellen selbst.
Hier verwenden an sich alle Fahrzeuge nahezu das gleiche Getriebe. Letztendlich unterscheiden sich die Getriebe der einzelnen Motoren nur in der Übersetzung und natürlich Manuelles Schaltgetriebe oder Automatikgetriebe.</p>
<p class="pp1">In jedem dieser Fälle kann das Öl seine wichtige Aufgabe nicht mehr zufriedenstellend ausführen. Die Reibung nimmt zu, der Wirkungsgrad des Getriebes ab. Der Fahrer kann dies anhand veränderter Schaltpunkte merken - Handschaltgetriebe können davon ebenso betroffen sein wie Automatikgetriebe.</p>
<p class="pp1">Reagieren Sie nicht, verschleißen die Getriebekomponenten vorzeitig und ein Getriebeschaden droht. Dann kann es teuer für Sie werden. Der Einsatz eines neuen Getriebes verursacht Werkstattkosten im vierstelligen Bereich.</p>
<p class="pp1"><strong>Mechanisches Getriebe</strong></p>
<p class="pp1">Mechanische Getriebe sind mit Getriebeöl oder Motoröl in einigen Autos geschmiert, die regelmäßig geändert werden sollen, obwohl nicht so häufig wie die Automatikgetriebeflüssigkeit in ähnlich ausgerüstetem Fahrzeug.</p>
<p class="pp1">Probleme, die durch den Ausfall von Getriebeöl verursacht sind:</p>
<ul>
    <li>Gangschaltung wird schwieriger geworden sein</li>
    <li>abnormale Schwingung vom Getriebekasten</li>
    <li>Getriebekastenschaden.</li>
</ul>
<p class="pp1">Empfohlene Intervall: Jede 60,000km oder 3 Jahre (je nachdem zuerst kommt).</p>
<p class="pp1"><strong>Differentialgetriebe (Hinterradantrieb und 4-Rad-Antrieb)</strong></p>
<p class="pp1">Das Ausgleichsgetriebe in einem Auto ist ein Getriebe, das hauptsächlich unterschiedliche Räder mit verschiedenen Geschwindigkeiten drehen kann. Da das Differential eine Vielzahl von beweglichen Teilen in Form von Rädern enthält, ist es wichtig, die Teile richtig zu schmieren, um die ordnungsgemäße Funktion zu ermöglichen und Schäden zu vermeiden.</p>
<p class="pp1">Differentialöl wurde speziell für diesen Zweck entwickelt.</p>
<p class="pp1">Probleme, die durch den Ausfall von Getriebeöl verursacht sind:</p>
<ul>
    <li>abnormale Schwingung vom Getriebekasten</li>
    <li>Beide Räder können einfach blockiert werden</li>
    <li>Differentialgetriebeschäden.</li>
</ul>
<p class="pp1">Empfohlene Intervall: Jede 40,000km oder 2 Jahre (je nachdem zuerst kommt).</p>
<p class="pp1"><strong>Automatikgetriebe</strong></p>
<p class="pp1">Automatikgetriebeflüssigkeit (ATF) ist ein Getriebeöl für automatisch betätigte Fahrzeuggetriebe, das sowohl zur Steuerung der hydraulischen Funktionen als auch zur Schmierung des Getriebes eingesetzt wird. Es ist typischerweise rot oder grün gefärbt, um es von Motoröl und anderen Flüssigkeiten in dem Fahrzeug zu unterscheiden. Bei den meisten Fahrzeugen wird sein Niveau mit einem Ölmeßstab überprüft, während der Motor läuft.</p>
<p class="pp1">Diese Flüssigkeit ist ein hochraffiniertes Mineralöl, das für die speziellen Anforderungen eines Automatikgetriebes, beispielsweise wie der Ventilbetätigung, der Bremsbandreibung und des Drehmomentwandlers sowie Getriebeschmierung optimiert ist.</p>
<p class="pp1">Synthetische ATF hat auch vor kurzem zur Verfügung gestanden, bietet dabei bessere Leistung und Lebensdauer für bestimmte Anwendungen (wie zB Anhängerbetrieb).</p>
<p class="pp1">ATF wird auch als Hydraulikmedium in einigen Servolenkungen und als Schmiermittel in einigen 4WD Verteilergetrieben eingesetzt.</p>
<p class="pp1">Probleme, die durch den Ausfall von Getriebeöl verursacht sind:</p>
<ul >
    <li>Auto-Schaltung wird schwieriger geworden sein</li>
    <li>Gangschaltung darf möglicherweise nicht richtig arbeiten</li>
    <li>“Kick-down” darf sich nicht richtig funktionieren</li>
    <li>Getriebekastenschaden.</li>
</ul>
<p class="pp1">Empfohlene Intervall: Jede 20,000km oder 1 Jahr (je nachdem zuerst kommt).</p>
<p class="pp1"><strong>Stufenlos verstellbares Getriebe (CVT)</strong></p>
<p class="pp1">CVT- Flüssigkeit (CVTF) enthält typischerweise eine Vielzahl von chemischen Verbindungen, die für die erforderliche Eigenschaften einer bestimmten CVTF Spezifikation bestimmt werden.</p>
<p class="pp1">Die meisten CVT-Flüssigkeiten enthalten einige Kombination von Rostschutzmittel, Antischaum-Additive, Detergenzien, Dispergatoren, Antiverschleißadditive, Antioxidationsverbindungen, Tenside, Kaltfließverbesserer, Hochtemperatur-Verdickungsmittel, Dichtungsanlagen und Erdöl-Farbstoff.</p>
<p class="pp1">Einige der Automobilhersteller können die ATF als CVT-Getriebe Schmiermittel verwenden.</p>
<p class="pp1">Während andere Autohersteller haben ihre eigene CVTF nur für das CVT-Getriebe.</p>
<p class="pp1">Probleme, die durch den Ausfall von Getriebeöl verursacht sind:</p>
<ul>
    <li>Auto-Schaltung wird schwieriger geworden sein</li>
    <li>abnormale Schwingung vom Getriebekasten</li>
    <li>Gangschaltung darf möglicherweise nicht richtig arbeiten</li>
    <li>“Kick-down” darf sich nicht richtig funktionieren</li>
    <li>Getriebekastenschaden.</li>
</ul>
<p class="pp1">Empfohlene Intervall:</p>
<ul>
    <li>Jede 10,000km oder 6 Monate (je nachdem zuerst kommt)</li>
    <li>Jede 20,000km oder 1 Jahr (je nachdem zuerst kommt)</li>
</ul>
<p class="pp1"><strong>Direktschaltgetriebe (DSG)</strong></p>
<p class="pp1">DSG-Getriebekasten arbeitet leistungsfähiger als mechanisches, sondern hat dabei auch glattere Gangschaltung im Vergleich zum Automatikgetriebe und CVT-Getriebekasten. DSG-Getriebekasten verfügt Naßkupplung Design. DSG Getriebeölwechselintervall ist nicht so häufige als Automatikgetriebe und CVT-Getriebe.</p>
<p class="pp1">Probleme, die durch den Ausfall von Getriebeöl verursacht sind:</p>
<ul >
    <li>((Bisher gibt es keine Aufzeichnung des Ausfalls von Getriebeöl außer dem Sensorausfall)</li>
</ul>
<p class="pp1">Empfohlene Intervall: Jede 40,000km oder 2 Jahre (je nachdem zuerst kommt).</p>
';
