<?php
$_['heading_title'] = 'FUNKTIONEN VON MOTORÖLEN';
$_['text_content'] = '
<p class="pp1">Motoröl übernimmt 6 wesentliche Funktionen. Die wichtigsten sind:</p>
<p class="pp1"><strong>1.1. Schmierung und Verschleißschutz</strong></p>
<p class="pp1">Im Motor agieren sehr viele bewegliche Teile miteinander. Durch Schmierung dieser Teile mit Öl wird deren Reibung reduziert und eine Verkantung verhindert. Das Öl bildet dabei einen permanenten Schmierfilm, der bei kalten Temperaturen möglichst dünnflüssig ist und bei hohen Temperaturen nicht abreißt. 
Dadurch werden die Kontaktflächen getrennt und eine Berührung sowie der daraus resultierende Verschleiß unterbunden.<br/>Darüber hinaus erfüllt Motoröl vier weitere wichtige Funktionen:</p>
<p class="pp2"><strong>2. Sauberhalten des Motorinneren</strong></p>
<p class="pp2">Motoröle haben des Weiteren die Aufgabe, den Motor sauber zu halten. Kleine Teilchen sowie Verbrennungsrückstände werden vom Öl aufgenommen, neutralisiert und somit in der Schwebe gehalten. Dies verringert den Kraftstoffverbrauch und die Schadstoffemission. Die Lebensdauer des Motors wird deutlich erhöht.</p>
<p class="pp2"><strong>3. Kühlung</strong></p>
<p class="pp2">Beim Betrieb eines Verbrennungsmotors entsteht Wärme. Motoröle haben die Aufgabe die besonders temperaturbelasteten Teile im Motorraum zu kühlen und so vor Verschleiß zu schützen.</p>
<p class="pp2"><strong>4. Korrosionsschutz</strong></p>
<p class="pp2">Bei der Korrosion reagiert ein Werkstoff auf seine Umgebung (chemisch oder elektrochemisch). Korrosionsschutz-Additive versuchen, das zu verhindern oder zu vermindern. Die bekannteste Art der Korrosion ist das Oxidieren von Metall – also das Rosten. Beim so genannten passiven Korrosionsschutz wird das Metall mit einer Schutzschicht überzogen, die feindlichen Stoffe, wie zum Beispiel Wasser abhalten soll.</p>
<p class="pp2"><strong>5. Kondensationsschutz</strong></p>
<p class="pp2">Kondenswasser entsteht bei jedem Motor während der Warmlaufphase und der Abkühlung nach dem Abstellen des Motors. Die Aufnahme des Wassers ins Motorenöl muss unbedingt vermieden werden, da dies zu einer Schaumbildung führt.</p>
';
