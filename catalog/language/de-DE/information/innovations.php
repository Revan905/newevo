<?php
$_['heading_title']    = 'INNOVATIONEN';
$_['text_content']     = '<p class="pp1">EVO Lubricants GmbH widmet besondere Aufmerksamkeit auf die Qualität des Produktes, und deshalb vervollkommnet sie die vorhandenen Technologien und sucht nach Innovationen. Die Ausarbeitung der neuen Ölformel dauert mehrere Monate der Laborversuche, Testen und Zertifizierung seiner Leistung noch vor der Herstellung.</p>
 <p class="pp1">In unserer Firma garantieren wir eine ausführliche Qualitätskontrolle des ganzen Herstellungszyklus der Schmiermaterialien, von der Kontrolle des Rohstoffes bis zur Lieferung der verpackten Produkte den Konsumenten. Alle Herstellungslinien sind mit den modernen automatischen Auswahlsystemen ausgerüstet, was eine 100% Kontrolle der Qualität versorgt.</p>
 <p class="pp1">Das Personal unseres Unternehmens beachtet aufmerksam auf die Veränderungen von Ölherstellern in der technischen Anforderungen der Fahrzeugen und arbeitet beständig die neue Schmiermaterialien, die nicht nur den gegenwärtigen Bedürfnissen entsprechen, sondern einen Schritt voraus sind, aus.</p><br/>';

