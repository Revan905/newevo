<?php
$_['heading_title'] = 'MOTORINDUSTRIE';
$_['text_content'] = '<br /><br />
<p class="pp1">Auf kaum eine technische Innovation ist die Menschheit heute so angewiesen wie auf das Automobil. Fast niemand mag noch darauf verzichten und ein Leben ohne Automobile wäre undenkbar. Als Automobil gelten übrigens alle Fahrzeuge, die sich ohne den Einsatz von Schienen und Muskelkraft selbstständig auf dem Land fortbewegen können.</p>
<ul>
    <li><p class="pp2"><strong>1839</strong> - wurde das erste Elektrofahrzeug von Schotten Robert Anderson gebaut.</p></li>
    <li><p class="pp2"><strong>1860</strong> - ließ sich der Franzose Etienne Lenoir einen Gasmotor patentieren.</p></li>
    <li><p class="pp2"><strong>1876</strong> - entwickelte Nikolaus August Otto den Viertaktmotor.</p></li>
    <li><p class="pp2"><strong>1886</strong> - beginnt die Geschichte des heutigen Automobils mit Verbrennungsmotor, der wurde von Carl Benz entwickelt.</p></li>
    <li><p class="pp2"><strong>Am 5. August 1888</strong> - unternahm Bertha Benz die erste Überlandfahrt von Mannheim nach Pforzheim. Dabei ging ihr das Benzin aus und sie musste „tanken“. Tankstellen gab es noch nicht. Die entsprechenden Chemikalien gab es nur in der Apotheke. So wurde die Stadtapotheke von Wiesloch zur ersten Tankstelle der Welt.</p></li>
    <li><p class="pp2"><strong>1897</strong> - 23 Jahre nach der Entwicklung des Ottomotors, entwickelte Rudolf Diesel den Dieselmotor.</p></li>
    <li><p class="pp2"><strong>1900</strong> - wurde der Vorderantrieb patentiert.</p></li>
    <li><p class="pp2"><strong>1901</strong> - Fredrick Lanchester patentierte die Scheibenbremse.</p></li>
    <li><p class="pp2"><strong>1903</strong> - entwickelte Mary Anderson den Scheibenwischer.</p></li>
    <li><p class="pp2"><strong>1913</strong> - erfand Henry Ford die Fließbandproduktion und so konnten Automobile in größeren Stückzahlen und vor allem auch deutlich günstiger hergestellt werden.</p></li>
    <li><p class="pp2"><strong>1923</strong> - Lkw mit Dieselmotor.</p></li>
    <li><p class="pp2"><strong>1933</strong> - wurde der Drehkolbenmotor erfunden.</p></li>
    <li><p class="pp2"><strong>1936</strong> - erster Pkw mit Dieselmotor Mercedes-Benz W138.</p></li>
    <li><p class="pp2"><strong>1940</strong> - gab es das erste Automatikgetriebe.</p></li>
    <li><p class="pp2"><strong>1948</strong> - wurde der Radialreifen (Gürtelreifen) erfunden.</p></li>
    <li><p class="pp2"><strong>1951</strong> - gab es sowohl den ersten Wagen mit Benzindirekteinspritzung als auch das erste Automobil mit Servolenkung.</p></li>
    <li><p class="pp2"><strong>1957</strong> - waren Beckensicherheitsgurte auf Wunsch in Fahrzeugen verfügbar.</p></li>
    <li><p class="pp2"><strong>1963</strong> - wurde das erste Auto mit einem Wankelmotor gebaut.</p></li>
    <li><p class="pp2"><strong>1967</strong> - wurde elektronische Benzineinspritzung erfunden.</p></li>
    <li><p class="pp2"><strong>1974</strong> - gab es die ersten Katalysatoren, entwickelt von General Motors.</p></li>
    <li><p class="pp2"><strong>1978</strong> - wurde das erste vollelektronische ABS (Anti-Blockier-System) von der Daimler Benz AG entwickelt.</p></li>
    <li><p class="pp2"><strong>1980</strong> - Airbag (schon seit den späten 1960er-Jahren in Entwicklung).</p></li>
    <li><p class="pp2"><strong>1990</strong> - gab es das erste Mal Katalysatoren für Fahrzeuge mit Dieselantrieb.</p></li>
    <li><p class="pp2"><strong>1995</strong> - wurde das ESP (Elektronisches Stabilitätsprogramm) entwickelt.</p></li>
    <li><p class="pp2"><strong>1997</strong> - wurden die ersten Hybridfahrzeuge mit einem kombinierten Elektro- und Verbrennungsantrieb vorgestellt.</p></li>
    <li><p class="pp2">Ab <strong> 2005 </strong>wurde vor allem Wert auf die Integration von Unterhaltungselektronik in das Interieur der Automobile gelegt und heute besitzen viele Neuwagen bereits ein integriertes Navigationssystem.</p></li>
    <li><p class="pp2"><strong>2010</strong> - wurde das “Easy Open”-System entwickelt.</p></li>
    <li><p class="pp2"><strong>2010</strong> - das weltweit erstes Tote-Winkel-System.</p></li>
    <li><p class="pp2"><strong>2011</strong> - das “Active City Stop” System im Ford Focus ist das erste Assistenzsystem, das Fahrer schon bei Geschwindigkeiten bis zu 30 km/h unterstützt.</p></li>
    <li><p class="pp2">Im Mai <strong>2014</strong> gab Google bekannt, dass 100 Testfahrzeuge gebaut werden sollen, bei denen auf Lenkrad, Bremse und Gaspedal verzichtet werden soll. Die Fahrzeuge sollen nicht in Privatbesitz wechseln sondern quasi als führerlose Taxis bzw. Transportkapseln dienen. In einem Video zeigt Google wie Privatpersonen den Prototyp testen. Google vereint damit die drei Prinzipien Elektroauto, autonomes Fahren und Car-Sharing (s.a. Autonomes Landfahrzeug). </p></li>
    <li><p class="pp2">Weitere Entwicklungsfelder sind der Fußgängerschutz, die Verwendung wiederverwertbarer Rohstoffe sowie Führerlose Fahrsysteme. General Motors plant erste unbemannte Pkw im Test ab 2015 und in der Serienproduktion ab 2018.</p></li>
</ul>
';
