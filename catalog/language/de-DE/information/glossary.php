<?php
$_['heading_title']    = 'GLOSSAR';
$_['text_content']     = '<p class="pp2"><strong>ACEA</strong> - Europäischer Automobilherstellerverband.</p>
<p class="pp2"> <strong>Additive</strong> - chemische Verbindung oder Verbindungen, die den Schmiermitteln hinzugefügt werden, um dabei bestimmte Eigenschaften zu erreichen oder die bestehenden zu verbessern.  </p>
<p class="pp2"><strong>API</strong> - Das American Petroleum Institute</p>
<p class="pp2"><strong>Aschegehalt</strong> - Gewichtsprozentsatz des Rückstandes nach der Verbrennung von Ölprobe (ASTM Methode D482).</p>
<p class="pp2"><strong>ASTM</strong> - Amerikanische Gesellschaft für Materialprüfung - ist eine internationale Standardisierungsorganisation, die technische Standards für Waren und Dienstleistungen veröffentlicht.</p>
<p class="pp2"><strong>Basisöl</strong> - ein vollraffiniertes Schmieröl, das ein Bestandteil von Schmiermittelformulierungen ist. Das “American Petroleum Institute” teilt Basisöle in fünf Gruppen ein:</p><br />
<table bgcolor="black" border="0" cellpadding="2" cellspacing="1" width="789" height="127">
<tbody>
<tr>
<td style="text-align: center; border: solid black 1px;">&nbsp;API’s Einteilung für Basisöle</td>
<td style="text-align: center; border: solid black 1px;">&nbsp;gesättigte<br>Verbindungen<br>ASTM D 2007<br>Gew.-%</td>
<td style="text-align: center; border: solid black 1px;">
<p>Schwefel</p>
<p>ASTM D 2622<br>Gew.-%</p>
</td>
<td style="text-align: center; border: solid black 1px;">Viskositäts-<br>Index<br>ASTM D 2270</td>
</tr>
<tr>
<td style="border: solid black 1px;">Gruppe I</td>
<td style="text-align: center; border: solid black 1px;">&lt;90</td>
<td style="text-align: center; border: solid black 1px;">&gt;0,03</td>
<td style="text-align: center; border: solid black 1px;">80 bis 120</td>
</tr>
<tr>
<td style="border: solid black 1px;">Gruppe II</td>
<td style="text-align: center; border: solid black 1px;">&nbsp;≥90</td>
<td style="text-align: center; border: solid black 1px;">≤0,03</td>
<td style="text-align: center; border: solid black 1px;">80 bis 120</td>
</tr>
<tr>
<td style="border: solid black 1px;">Gruppe III</td>
<td style="text-align: center; border: solid black 1px;">≥90</td>
<td style="text-align: center; border: solid black 1px;">≤0,03</td>
<td style="text-align: center; border: solid black 1px;">≥120</td>
</tr>
<tr>
<td style="border: solid black 1px;">Gruppe IV</td>
<td colspan="3" style="text-align: center; border: solid black 1px;">Polyalphaolefine</td>
</tr>
<tr>
<td style="border: solid black 1px;">Gruppe&nbsp;V</td>
<td colspan="3" style="text-align: center; border: solid black 1px;">Alle anderen Basisöle, die nicht zu Gruppe I oder Gruppe IV gehören</td>
</tr>
</tbody>
</table><br />
<p class="pp2"><strong>Siedepunkt</strong> - Est eine Temperatur, bei der ein Stoff vom flüssigen in den gasförmigen Aggregatzustand übergeht. Mit anderen Worten, am Siedepunkt hat der Dampfdruck eines Stoffes den Umgebungsdruck erreicht. Somit ist der Siedepunkt von zwei Faktoren abhängig. Zum einen vom Stoff selbst und zum andern vom Umgebungsdruck. Je höher der Umgebungsdruck ist, desto höher die Temperatur bei dem der Siedepunkt des Stoffs erreicht ist. </p>
<p class="pp2"><strong>Centipoise (cP.)</strong> - die herkömmliche Einheit, um dynamische Viskosität anzugeben. 1 centipoise = 0.01 poise.</p>
<p class="pp2"><strong>Centistoke (cSt.)</strong> - die Standardeinheit zur Messung der kinematischen Viskosität. 1 centistoke = 0.01 stoke.</p>
<p class="pp2"><strong>Diester-Öl </strong> - ein synthetisches Schmiermittel, das aus Estern hergestellt wird; ist auch Esteröl genannt.</p>
<p class="pp2"><strong>DIN </strong> - Deutsches Institut für Normung e.V.</p>
<p class="pp2"><strong>Dispergierung</strong> - wird in der Schmierung regelmässig austauschbar mit dem Reinigungsmittel verwendet; ein Additiv, das die feine Teilchen der unlöslichen Materialien in einer homogenen Lösung hält; daher werden die Teilchen nicht absetzen und akkumulieren.</p>
<p class="pp2"><strong>EOLCS </strong> - Zulassungs- und Zertifizierungssystem für Motoröle des API.</p>
<p class="pp2"><strong>Flammpunkt </strong> - die niedrigste Temperatur eines Öles, bei der erstmalig entzündungsfähige Dampf-Luft-Gemische entstehen.</p>
<p class="pp2"><strong>Schaum </strong> - (von lateinisch spuma) sind gasförmige Bläschen, die von festen oder flüssigen Wänden eingeschlossen sind.</p>
<p class="pp2"><strong>VKA </strong> -  wird vielfach in der Schmierstoffindustrie verwendet und dort routinemäßig zur Produktentwicklung und Qualitätskontrolle eingesetzt.
Der Schmierstoff wird in einem Vierkugel-System geprüft, bestehend aus einer rotierenden Kugel (Laufkugel), die unter wählbaren Lasten (Prüfkräften) auf drei ihr gleichen Kugeln (Standkugeln) gleitet. Die Prüfkraft wird stufenweise gesteigert, bis ein Verschweißen des Vierkugel-Systems (untere Abbildung) eintritt.</p>
<p class="pp2"><strong>ILSAC </strong> - Internationaler Normungs- und Zulassungsausschuss für Schmierstoffe.</p>
<p class="pp2"><strong>JAMA</strong> - Japanischer Verband der Automobilhersteller.</p>
<p class="pp2"><strong>JASO</strong> - Japanische Normungsorganisation für Kraftfahrzeuge.</p>
<p class="pp2"><strong>JSAE</strong> - Japanischer Ingenieursverband der Automobilbranche.</p>
<p class="pp2"><strong>Kinematische Viskosität (n)</strong> - ist ein Ausdruck für die innere Reibung einer Flüssigkeit. Die kinematische Viskosität wird errechnet, indem man die dynamische Viskosität durch die Dichte einer Flüssigkeit teilt (n=h/r).</p>
<p class="pp2"><strong>Lack</strong> - ist ein flüssiger oder auch pulverförmiger Beschichtungsstoff, der dünn auf Gegenstände aufgetragen wird und durch chemische oder physikalische Vorgänge zu einem durchgehenden, festen Film aufgebaut wird.</p>
<p class="pp2"><strong>Mehrbereichsöle</strong> - Diese basieren auf dünnflüssigen Grundölen und werden mit speziellen Additive (z. B. Polymere wie Polyester oder Polyisobutylen) so gemischt, dass ihre Viskosität bei höheren Temperaturen nur geringfügig abnimmt. So kann dasselbe Öl im Sommer- wie im Winterbetrieb verwendet werden.</p>
<p class="pp2"><strong>NLGI</strong> - US - amerikanisches Institut für Schmierstoffe.</p>
<p class="pp2"><strong>OEM</strong> - Originalausrüstungshersteller.</p>
<p class="pp2"><strong>Oxidationsstabilität</strong> - bezeichnet die Widerstandsfähigkeit von Ölen gegenüber Oxidationsprozessen.</p>
<p class="pp2"><strong>Pour Point</strong> - (engl. „pour“ für „gießen“ im Sinne von „eingießen“) bezeichnet nach DIN 51597 für ein flüssiges Produkt die Temperatur, bei der es bei Abkühlung gerade noch fließt. Die Temperatur, bei der das zuvor flüssige Produkt erstarrt, ist der Stockpunkt.</p>
<p class="pp2"><strong>SAE</strong> - Vereinigung der Automobilingenieure.</p>
<p class="pp2"><strong>SAE-Viskositätsklassen</strong> - sind nur ein Maß für die Zähflüssigkeit der Schmieröle und geben keine Aussage über die Qualität und Zusammensetzung der Motoren- und Getriebeöle. SAE Viskositätsklasse:</p>
<p class="pp2"><strong>Schlamm </strong> - Unlösliches Material, das infolge der Verschlechterungsreaktionen in einem Öl oder durch die Ölverschmutzung, und sogar dank diesen beiden Prozessen, gebildet wird.</p>
<p class="pp2"><strong>Gesamtbasenzahl (TBN)</strong> - Abkürzung für "Total Base Number", ein Maß für die Menge an Säuren, die durch die Basizität eines Öls neutralisiert werden kann. Wird in Milligramm Kaliumhydroxid je Gramm Schmierstoff (mg KOH/g) angegeben.</p>
<p class="pp2"><strong>Viskosität</strong> -  ist ein Maß für die Zähflüssigkeit eines Fluids. Der Kehrwert der Viskosität ist die Fluidität, ein Maß für die Fließfähigkeit eines Fluids. Je größer die Viskosität, desto dickflüssiger (weniger fließfähig) ist das Fluid; je niedriger die Viskosität, desto dünnflüssiger (fließfähiger) ist es, kann also bei gleichen Bedingungen schneller fließen.</p>
<p class="pp2"><strong>Viskositätsindex (VI)</strong> - beschreibt die Temperaturabhängigkeit der kinematischen Viskosität eines Schmieröls. Mit dem Viskositätsindex (VI) wird das Viskositäts-Temperatur-Verhalten (VT-Verhalten) eines Schmieröls bestimmt. Öle haben die Eigenschaft, ihre Viskosität (Fließverhalten) bei Temperaturänderung zu verändern. Bei steigender Temperatur verringert sich die Viskosität - das Öl wird dünnflüssiger - und bei fallender Temperatur erhöht sich die Viskosität - das Öl wird dickflüssiger.</p>
';
