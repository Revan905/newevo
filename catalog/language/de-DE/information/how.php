<?php
$_['heading_title'] = 'WAS BEDEUTET DIE MARKIERUNG AUF DER DOSE?';
$_['text_content'] = '
<p class="pp2">Das API - "American Petroleum Institute"  spezifiziert die Qualität des Öls.</p>
<p class="pp2">Das API legt die Klassifizierung aufgrund der Betriebsbedingungen der Motoren, getrennt nach Otto- und Dieselmotoren fest. Damit bestimmen die Motorenhersteller motorenseitig und die Ölgesellschaften schmierstoffseitig die erforderliche bzw. die gebotene Klasse. Die Bezeichnung API ist auf jedem Motorenölgebinde zu finden.</p>
<p class="pp2">Der erste Buchstabe ist immer ein S oder C.</p>
<ul>
<li><p class="pp2">API S - Steht für Ottomotoren (Service-Klassen auch Spark Ignition)</p> </li>
<li><p class="pp2">API C - Steht für Dieselmotoren (Commercial-Klassen auch Compression Ignition)</p> </li>
</ul>
<p class="pp2">Der zweite Buchstabe steht für die Klasse (Qualität). Je höher der Buchstabe im Alphabet, je besser die Qualität. Die derzeit höchsten API Spezifikationen sind SN / CJ - 4.</p>
<p class="pp2">Die Angabe 5W-30 auf der Dose kennzeichnet die SAE-Klasse ("Society of Automotive Engineers"). Sie beschreibt die Viskosität ( - ist das Maß für die Fließeigenschaften des Öls). Die vor dem Buchstaben "W" (für Winter) gekennzeichnete Zahl beschreibt die Fließeigenschaften bei Kälte. Je kleiner die Zahl, desto schneller und besser fließt das Öl bei Kälte an die wichtigen Schmierstellen im Motor. Die Zahl ohne "W" beschreibt das Fließverhalten bei einer Temperatur von 100 Grad.</p>
<p class="pp2">RESOURCE CONSERVING - bewertet als resourcesparend nach dem Testergebnisse in der Amerikanische Gesellschaft für Materialprüfung - eine internationale Standardisierungsorganisation, die technische Standards für Waren und Dienstleistungen veröffentlicht. Die "Resource Conserving“  erfordert weitere Eigenschaften in Vergleich zu der ehemaligen zusätzlichen energiesparenden Kategorie - die "Energy Conserving", wie: Emissionssystemschutz, Turboladerschutz, Kompatibilität mit Motoren, die auf Ethanol haltigen Brennstoffen, bis E-85.</p>
';
