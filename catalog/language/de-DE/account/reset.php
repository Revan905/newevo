<?php
/**
 * @version		$Id: reset.php 4344 2016-06-01 10:19:06Z mic $
 * @package		Translation Deutsch Frontend
 * @author		mic - http://osworx.net
 * @copyright	2016 OSWorX - https://osworx.net
 * @license		GPL - www.gnu.org/copyleft/gpl.html
 */

// header
$_['heading_title']		= 'PAsswort wieder herstellen';

// Text
$_['text_account']		= 'Konto';
$_['text_password']		= 'Neues Passwort eingeben';
$_['text_success']		= 'Passwort wurde erfolgreich aktualisiert';

// Entry
$_['entry_password']	= 'Passwort';
$_['entry_confirm']		= 'Wiederholung';

// Error
$_['error_password']	= 'Passwort muss zwischen 4 und 20 Zeichen lang sein';
$_['error_confirm']		= 'Passwort und Wiederholung stimmen nicht überein';
$_['error_code']		= 'Wiederherstellungscode ist entweder ungültig oder wurde schon einmal verwendet';