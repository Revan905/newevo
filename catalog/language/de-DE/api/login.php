<?php
/**
 * @version		$Id: login.php 4343 2016-06-01 10:18:23Z mic $
 * @package		Language Translation german
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		GPL - www.gnu.org/copyleft/gpl.html
 */

// Text
$_['text_success']	= 'API-Sitzung erfolgreich gestartet';

// Error
$_['error_key']		= 'Falscher API-SChlüssel';
$_['error_ip']		= 'Diese IP-Adresse %s hat keinen Zugriff auf die API!';
	// older < 2.1.x
$_['error_login'] 	= 'Hinweis: kein Treffer für Name und/oder Passwort';