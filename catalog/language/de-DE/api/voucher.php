<?php
/**
 * @version		$Id: voucher.php 4343 2016-06-01 10:18:23Z mic $
 * @package		Language Translation german
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		GPL - www.gnu.org/copyleft/gpl.html
 */

// Text
$_['text_success']		= 'Gutscheinsumme erfolgreich angewendet';
$_['text_cart']			= 'Warenkorb wurde erfolgreich aktualisiert';
$_['text_for']			= '%s Geschenksgutschein für %s';

// Error
$_['error_permission']	= 'Keine Rechte für diese Aktion';
$_['error_voucher']		= 'Achtung: Gutschein ist entweder ungültig oder Betrag wurde bereits eingelöst';
$_['error_to_name']		= 'Empfängername muss zwischen 1 und 64 Zeichen lang sein';
$_['error_from_name']	= 'Der eigene Name muss zwischen 1 und 64 Zeichen lang sein';
$_['error_email']		= 'Emailadresse ist nicht gültig';
$_['error_theme']		= 'Es muss eine Vorlage ausgewählt werden';
$_['error_amount']		= 'Betrag muss zwischen %s und %s liegen';