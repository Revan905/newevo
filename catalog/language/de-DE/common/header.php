<?php
/**
 * @version		$Id: header.php 3721 2014-08-18 13:37:29Z mic $
 * @package		Translation Deutsch
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		GPL - www.gnu.org/copyleft/gpl.html
 */

// Text
$_['text_home']			= 'Start';
$_['text_wishlist']		= 'Wunschliste (%s)';
$_['text_shopping_cart']= 'Warenkorb';
$_['text_category']		= 'Kategorien';
$_['text_account']		= 'Mein Konto';
$_['text_register']		= 'Registrierung';
$_['text_login']		= 'Anmelden';
$_['text_order']		= 'Auftragsverlauf';
$_['text_transaction']	= 'Transaktionen';
$_['text_download']		= 'Downloads';
$_['text_logout']		= 'Abmelden';
$_['text_checkout']		= 'Kasse';
$_['text_search']		= 'Suche';
$_['text_company']      = 'Firma';
$_['text_products']     = 'Produkte';
$_['text_oilselector']  = 'Schmierindex';
$_['text_technlogies']  = 'Technologie';
$_['text_all']			= 'Alle';
$_['text_our_company']  = 'Unsere firma';
$_['text_production']   = 'Herstellung';
$_['text_innovations']  = 'Innovationen';
$_['text_technology_iron_defence']  = 'Technologie des Eisenschutzes';
$_['text_glossary']  = 'Glossar';
$_['text_motor_industry']   = 'Motorindustrie';
$_['text_motor_oil']   = 'Motoröl';
$_['text_approvals']   = 'Zulassungen';
$_['text_optimal_oil_drain_intervals']   = 'Optimale Interwale des Ölersatzes';
$_['text_useful_links']   = 'Nützliche Links';
$_['text_what_does_motor_oil_do']   = 'Funktionen der Motoröl';
$_['text_how_to_read_oil_can']   = 'Was bedeutet die Markierung auf der Dose?';
$_['text_engine_oil_change_interval']   = 'Motorölwechselintervall';
$_['text_gear_oil_change_interval']   = 'Getriebeölwechselintervall';