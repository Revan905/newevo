<?php
/**
 * @version		$Id: password.php 4343 2016-06-01 10:18:23Z mic $
 * @package		Translation Deutsch Frontend
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		GPL - www.gnu.org/copyleft/gpl.html
 */

// Heading
$_['heading_title']		= 'Passwort ändern';

// Text
$_['text_account']		= 'Konto';
$_['text_password']		= 'Passwort';
$_['text_success']		= 'Passwort wurde aktualisiert';

// Entry
$_['entry_password']	= 'Passwort';
$_['entry_confirm']		= 'Passwortwiederholung';

// Error
$_['error_password']	= 'Passwort muss zwischen 4 und 20 Zeichen lang sein';
$_['error_confirm']		= 'Passwort und Wiederholung stimmen nicht überein';